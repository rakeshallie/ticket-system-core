﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketingSystem.Models;

namespace TicketingSystem.Web.ViewModels.Home
{
    public interface IHomeServices
    {
        IList<Ticket> GetIndexViewModel(int numberOfTickets);
    }
}
