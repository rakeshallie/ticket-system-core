﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketingSystem.Models;

namespace TicketingSystem.Web.Models
{
    public interface ITicketSystemData
    {
        DbContext Context { get; }

        IRepository<Category> Categories { get; }

        IRepository<Comment> Comments { get; }

        IRepository<Image> Images { get; }

        IRepository<Ticket> Tickets { get; }

        IRepository<ApplicationUser> Users { get; }

        void Dispose();

        int SaveChanges();
    }
}
