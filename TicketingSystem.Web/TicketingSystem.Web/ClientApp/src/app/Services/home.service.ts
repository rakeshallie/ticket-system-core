import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private homeUrl: string = 'api/Home';
  constructor(private http: HttpClient) { }

  getTicket() {
    return this.http.get(this.homeUrl);
  }

  getTicketById(data: any) {
    return this.http.get(this.homeUrl + "\/" + data);
  }

}
