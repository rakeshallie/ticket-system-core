import { Component, OnInit } from '@angular/core';
import { HomeService } from '../Services/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  Model: any;
  constructor(private homeService: HomeService) {

  }
  ngOnInit(): void {
    this.homeService.getTicket().subscribe(Model => this.Model = Model);
  }
}
