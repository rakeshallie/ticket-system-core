import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HomeService } from '../Services/home.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {
  id: any;
  Model: any;
  constructor(private http: HttpClient, private homeServices: HomeService, private router: ActivatedRoute) { }

  ngOnInit() {
    this.router.paramMap.subscribe(params => {
      this.id = +params.get('id');
    });
    this.homeServices.getTicketById(this.id).subscribe(Model => this.Model = Model);
  }
}
