﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketingSystem.Models;
using TicketingSystem.Web.Models;
using TicketingSystem.Web.ViewModels.Home;

namespace TicketingSystem.Web.Infrastructure.Services
{
    public class HomeServices : BaseServices, IHomeServices
    {
        public HomeServices(ITicketSystemData data)
            : base(data)
        {
        }

        public IList<Ticket> GetIndexViewModel(int numberOfTickets)
        {
            var indexViewModel = this.Data
                .Tickets
                .All()
                .OrderByDescending(t => t.Comments.Count())
                .Take(numberOfTickets)
                .ToList();

            return indexViewModel;
        }
    }
}
