﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketingSystem.Web.Models;

namespace TicketingSystem.Web.Infrastructure.Services
{
    public abstract class BaseServices
    {
        protected ITicketSystemData Data { get; private set; }

        public BaseServices(ITicketSystemData data)
        {
            this.Data = data;
        }
    }
}
