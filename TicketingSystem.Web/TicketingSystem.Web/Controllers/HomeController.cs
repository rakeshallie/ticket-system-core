﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TicketingSystem.Common;
using TicketingSystem.Data;
using TicketingSystem.Models;
using TicketingSystem.Web.Models;
using TicketingSystem.Web.ViewModels.Home;

namespace TicketingSystem.Web.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext applicationDbContext;
        private ITicketSystemData dbContextOptions;
        private IHomeServices homeServices;
        private IRandomGenerator random;
        public HomeController(ITicketSystemData dbContextOptions, IHomeServices homeServices, IRandomGenerator random, ApplicationDbContext applicationDbContext)
        {
           
            this.homeServices = homeServices;
            this.dbContextOptions = dbContextOptions;
            this.random = random;
            this.applicationDbContext = applicationDbContext;

            
        }

        [Route("api/home")]
        public IList<Ticket> Index()
        {
            if (GlobalConstants.isFirst)
            {
                Seeding();
                GlobalConstants.isFirst = false;
            }

            return this.homeServices.GetIndexViewModel(6);
        }

        [HttpGet("api/Home/{id}")]
        public Ticket Index(int id)
        {
            return dbContextOptions.Tickets.GetById(id);
        }

        //private Image GetSampleImage()
        //{
        //    var directory = AssemblyHelpers.GetDirectoryForAssembyl(Assembly.GetExecutingAssembly());
        //    var file = System.IO.File.ReadAllBytes(directory + "/Migrations/Imgs/cat.jpg");
        //    var image = new Image
        //    {
        //        Content = file,
        //        FileExtension = "jpg"
        //    };

        //    return image;
        //}
        public void Seeding()
        {
            for (int i = 0; i < 10; i++)
            {
                var user = new ApplicationUser
                {
                    Email = string.Format("{0}@{1}.com", this.random.RandomString(6, 16), this.random.RandomString(6, 16)),
                    UserName = this.random.RandomString(6, 16)
                };

                applicationDbContext.Users.Add(user);
            }

            var adminUser = new ApplicationUser
            {
                Email = "admin@mysite.com",
                UserName = "Administrator"
            };

            applicationDbContext.Users.Add(adminUser);
            applicationDbContext.SaveChanges();
            //var image = this.GetSampleImage();
            var users = applicationDbContext.Users.Take(10).ToList();
            for (int i = 0; i < 5; i++)
            {
                var category = new Category
                {
                    Name = this.random.RandomString(5, 20)
                };

                for (int j = 0; j < 10; j++)
                {
                    var ticket = new Ticket
                    {
                        Author = users[this.random.RandomNumber(0, users.Count - 1)],
                        Title = this.random.RandomString(5, 40),
                        Description = this.random.RandomString(200, 500),
                        //Image = image,
                        Priority = (PriorityType)this.random.RandomNumber(0, 2)
                    };

                    for (int k = 0; k < 5; k++)
                    {
                        var comment = new Comment
                        {
                            Author = users[this.random.RandomNumber(0, users.Count - 1)],
                            Content = this.random.RandomString(100, 200)
                        };

                        ticket.Comments.Add(comment);
                    }

                    category.Tickets.Add(ticket);
                }

                dbContextOptions.Categories.Add(category);
                dbContextOptions.SaveChanges();
            }
        }
    }
}
