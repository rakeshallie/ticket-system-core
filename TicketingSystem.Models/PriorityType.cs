﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicketingSystem.Models
{
    public enum PriorityType
    {
        Low = 0,
        Medium = 1,
        High = 2
    }
}
